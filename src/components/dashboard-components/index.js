import Statistics from './statistics/index.jsx';
import SalesSummary from './sales-summary/sales-summary.jsx';
import EmailCampaign from './email-campaign/email-campaign.jsx';
import ActiveVisitors from './active-visitors/active-visitors.jsx';
import Stats from './stats/stats.jsx';
import Projects from './projects/projects.jsx';
import RecentComment from './recent-comment/recent-comment.jsx';
import Chat from './chat/chat.jsx';
import CryptoCoins from './crypto-coins/crypto-coins.jsx';
import CryptoChart from './crypto-chart/crypto-chart.jsx';
import CryptoOrder from './crypto-order/crypto-order.jsx';
import CryptoExchange from './crypto-exchange/crypto-exchange.jsx';
import CryptoTrade from './crypto-trade/crypto-trade.jsx';
import CryptoTrade2 from './crypto-trade2/crypto-trade2.jsx';
import CryptoMarket from './crypto-market/crypto-market.jsx';
import Earnings from './earnings/earnings.jsx';
import EarningsOverview from './earnings-overview/earnings-overview.jsx';
import Products from './products/products.jsx';
import Sales from './sales/sales.jsx';
import Orders from './orders/orders.jsx';
import Reviews from './reviews/reviews.jsx';
import CampaignOverview from './campaign-overview/campaign-overview.jsx';
import SalesRatio from './sales-ratio/sales-ratio.jsx';
import Visits from './visits/visits.jsx';
import OrderStatus from './order-status/order-status.jsx';
import TaskList from './tasklist/tasklist.jsx';
import UserProfile from './user-profile/user-profile.jsx';
import UserProfile2 from './user-profile2/user-profile2.jsx';
import BrowserStats from './browser-stats/browser-stats.jsx';
import Feeds from './feeds/feeds.jsx';
import Weather from './weather/weather.jsx';

export {
    Statistics,
    SalesSummary,
    EmailCampaign,
    ActiveVisitors,
    Stats,
    Projects,
    RecentComment,
    Chat,
    CryptoCoins,
    CryptoChart,
    CryptoOrder,
    CryptoExchange,
    CryptoTrade,
    CryptoTrade2,
    CryptoMarket,
    Earnings,
    EarningsOverview,
    Products,
    Sales,
    Orders,
    Reviews,
    CampaignOverview,
    SalesRatio,
    Visits,
    OrderStatus,
    TaskList,
    UserProfile,
    UserProfile2,
    BrowserStats,
    Feeds,
    Weather
};